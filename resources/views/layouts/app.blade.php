<html>
<head>
    <title>Sperloos test</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

    <div class="container">
        @if (auth()->user())
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="{{route('dashboardPage')}}">Dashboard</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link btn btn-success btn-sm" href="{{route('addPostForm')}}">Add Post</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link btn btn-danger btn-sm" href="{{route('logOut')}}">Logout</a>
                    </li>
                </ul>
            </div>
        </nav>
        @endif
        @yield('content')
    </div>
</body>
</html>
