@extends('layouts.app')
@section('content')
    @if($post->hasMedia('thumbnail'))
        <img src="{{$post->getMedia('thumbnail')[0]->getFullUrl()}}">
    @endif
    <h1>{{$post->title}}</h1>
    <p>{{$post->content}}</p>
@endsection
