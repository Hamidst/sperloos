@extends('layouts.app')
@section('content')
    <main class="post-form">
        <div class="cotainer">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <h3 class="card-header text-center">Edit Post</h3>
                        <div class="card-body">
                            <form enctype="multipart/form-data" method="POST" action="{{ route('editPost', ['post_id' => $post->id]) }}">
                                @csrf
                                <div class="form-group mb-3">
                                    <input type="file" name="thumbnail">
                                </div>
                                <div class="form-group mb-3">
                                    <input
                                        type="text"
                                        placeholder="Title"
                                        id="title"
                                        class="form-control"
                                        value="{{$post->title}}"
                                        name="title"
                                        required
                                        autofocus>
                                    @if ($errors->has('title'))
                                        <span class="text-danger">{{ $errors->first('title') }}</span>
                                    @endif
                                </div>

                                <div class="form-group mb-3">
                                    <textarea placeholder="Post Content" id="content" class="form-control" name="content" required>
                                        {{$post->content}}
                                    </textarea>
                                    @if ($errors->has('content'))
                                        <span class="text-danger">{{ $errors->first('content') }}</span>
                                    @endif
                                </div>

                                <div class="d-grid mx-auto">
                                    <button type="submit" class="btn btn-dark btn-block">Save</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
