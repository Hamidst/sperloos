@extends('layouts.app')
@section('content')

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif

<div class="table">
    <table class="table">
        <thead>
            <tr>
                <th>id</th>
                <th>title</th>
                <th>user</th>
                <th>Thumb</th>
                <th>operations</th>
            </tr>
        </thead>
        <tbody>
            @foreach($posts as $post)
                <tr>
                    <td>{{$post->id}}</td>
                    <td>{{$post->title}}</td>
                    <td>{{$post->user->name}}</td>
                    <td>
                        @if($post->getFirstMediaUrl('thumbnail','thumbnail100'))
                            <img src="{{$post->getFirstMediaUrl('thumbnail','thumbnail100')}}">
                        @else
                            No Thumb
                        @endif
                    </td>
                    <td>
                        <form method="post" action="{{route('deletePost', ['post_id' => $post->id])}}">
                            {{ csrf_field() }}
                            <input type="submit" value="del" class="btn btn-sm btn-danger">
                        </form>
                        <a href="{{route('editPostForm', ['post_id' => $post])}}" class="btn btn-primary btn-sm">Edit</a>
                        <a href="{{route('showPost', ['post_id' => $post])}}" class="btn btn-success btn-sm">Show</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
