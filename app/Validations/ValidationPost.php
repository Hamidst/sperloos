<?php


namespace App\Validations;


use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;

/**
 * Class ValidationPost
 * @package App\Validations
 */
class ValidationPost
{
    /** @var Factory $factory */
    private $factory;

    /**
     * ValidationPost constructor.
     * @param Factory $factory
     */
    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param array $input_data
     * @throws ValidationException
     */
    public function updateAndSave(array $input_data)
    {
        $this->factory->validate($input_data, [
            'title' => 'required',
            'content' => 'required'
        ]);
    }

}
