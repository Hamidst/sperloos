<?php


namespace App\Validations;


use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;

/**
 * Class ValidationUser
 * @package App\Validations
 */
class ValidationUser
{
    /** @var Factory $factory */
    private $factory;

    /**
     * ValidationUser constructor.
     * @param Factory $factory
     */
    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param array $input_data
     * @throws ValidationException
     */
    public function login(array $input_data)
    {
        $this->factory->validate($input_data, [
            'email'     => 'required|email',
            'password'  => 'required|min:8'
        ]);
    }

}
