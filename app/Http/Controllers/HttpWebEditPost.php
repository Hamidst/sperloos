<?php
namespace App\Http\Controllers;


use App\Http\Actions\posts\ActionPostsUpdate;
use App\Http\Actions\posts\ActionUploadPostThumbnail;
use App\Validations\ValidationPost;
use Illuminate\Http\RedirectResponse;
use Illuminate\Validation\ValidationException;

/**
 * Class HttpWebEditPost
 * @package App\Http\Controllers
 */
class HttpWebEditPost
{
    /** @var ActionPostsUpdate $action_posts_update */
    private $action_posts_update;
    /** @var ValidationPost $validation */
    private $validation;
    /** @var ActionUploadPostThumbnail $action_upload_post_thumbnail */
    private $action_upload_post_thumbnail;

    /**
     * HttpWebEditPost constructor.
     * @param ActionPostsUpdate $action_posts_update
     * @param ValidationPost $validation
     * @param ActionUploadPostThumbnail $action_upload_post_thumbnail
     */
    public function __construct(ActionPostsUpdate $action_posts_update,
                                ValidationPost $validation,
                                ActionUploadPostThumbnail $action_upload_post_thumbnail)
    {
        $this->action_posts_update = $action_posts_update;
        $this->validation = $validation;
        $this->action_upload_post_thumbnail = $action_upload_post_thumbnail;
    }

    /**
     * @param int $post_id
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function __invoke(int $post_id)
    {
        #1 Validating request
        $this->validation->updateAndSave(request()->all());

        #2 Update Post
        if ($post = $this->action_posts_update->__invoke($post_id, request()->only(['title', 'content']))) {

            #3 Store thumbnail if available
            if(request()->hasFile('thumbnail') && request()->file('thumbnail')->isValid()){
                $this->action_upload_post_thumbnail->__invoke($post, request()->file('thumbnail'));
            }
            return redirect('dashboard')->with('message', 'Post Edited!');
        }
    }
}
