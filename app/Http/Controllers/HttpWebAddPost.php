<?php


namespace App\Http\Controllers;


use App\Http\Actions\posts\ActionPostsSave;
use App\Http\Actions\posts\ActionUploadPostThumbnail;
use App\Validations\ValidationPost;
use Illuminate\Http\RedirectResponse;
use Illuminate\Validation\ValidationException;

/**
 * Class HttpWebAddPost
 * @package App\Http\Controllers
 */
class HttpWebAddPost
{
    /** @var ActionPostsSave $action_posts_save */
    private $action_posts_save;
    /** @var ValidationPost $validation */
    private $validation;
    /** @var ActionUploadPostThumbnail $action_upload_post_thumbnail */
    private $action_upload_post_thumbnail;

    /**
     * HttpWebAddPost constructor.
     * @param ActionPostsSave $action_posts_save
     * @param ValidationPost $validation
     * @param ActionUploadPostThumbnail $action_upload_post_thumbnail
     */
    public function __construct(ActionPostsSave $action_posts_save,
                                ValidationPost $validation,
                                ActionUploadPostThumbnail $action_upload_post_thumbnail)
    {
        $this->action_posts_save = $action_posts_save;
        $this->validation = $validation;
        $this->action_upload_post_thumbnail = $action_upload_post_thumbnail;
    }

    /**
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function __invoke()
    {
        #1 Validating request
        $this->validation->updateAndSave(request()->all());

        #2 Store post
        if ($post = $this->action_posts_save->__invoke(request()->only(['title', 'content']))) {

            #3 Upload post thumbnail and generate conversions if available
            if(request()->hasFile('thumbnail') && request()->file('thumbnail')->isValid()){
                $this->action_upload_post_thumbnail->__invoke($post, request()->file('thumbnail'));
            }
            return redirect('dashboard')->with('message', 'Post Added!');
        }
    }
}
