<?php


namespace App\Http\Controllers;


use App\Http\Actions\ActionUserLogin;
use App\Validations\ValidationUser;

class HttpWebLogin
{
    /** @var ActionUserLogin */
    private $action_user_login;
    /** @var ValidationUser $validator */
    private $validator;

    /**
     * HttpWebLogin constructor.
     * @param ActionUserLogin $action_user_login
     */
    public function __construct(ActionUserLogin $action_user_login, ValidationUser $validator)
    {
        $this->action_user_login = $action_user_login;
        $this->validator = $validator;
    }

    public function __invoke()
    {
        #1 validate login request
        $this->validator->login(request()->all());

        #2 do login attempt
        if ($this->action_user_login->__invoke(request()->only(['email', 'password']))) {
            return redirect()->intended('dashboard');
        } else {
            return redirect("login");
        }
    }
}
