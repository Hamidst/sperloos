<?php


namespace App\Http\Controllers;


use App\Http\Actions\posts\ActionPostsDelete;

/**
 * Class HttpWebDeletePost
 * @package App\Http\Controllers
 */
class HttpWebDeletePost
{
    /** @var ActionPostsDelete $action_posts_delete */
    private $action_posts_delete;

    /**
     * HttpWebDeletePost constructor.
     * @param ActionPostsDelete $action_posts_delete
     */
    public function __construct(ActionPostsDelete $action_posts_delete)
    {
        $this->action_posts_delete = $action_posts_delete;
    }

    /**
     * @param int $post_id
     * @return
     */
    public function __invoke(int $post_id)
    {
        $msg = '';
        if ($this->action_posts_delete->__invoke($post_id)) {
            $msg = 'deleted';
        }

        return redirect('dashboard')->with('message', 'Post Removed!');
    }
}
