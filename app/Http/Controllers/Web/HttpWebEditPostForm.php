<?php


namespace App\Http\Controllers\Web;


use App\Http\Actions\posts\ActionPostGet;
use App\Models\Post;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Gate;

/**
 * Class HttpWebEditPostForm
 * @package App\Http\Controllers\Web
 */
class HttpWebEditPostForm
{
    /** @var ActionPostGet $action_get_post */
    private $action_get_post;

    /**
     * HttpWebEditPostForm constructor.
     * @param ActionPostGet $action_get_post
     */
    public function __construct(ActionPostGet $action_get_post)
    {
        $this->action_get_post = $action_get_post;
    }

    /**
     * @param int $post_id
     * @return Application|Factory|View
     */
    public function __invoke(int $post_id)
    {
        $post = $this->action_get_post->__invoke($post_id);
        return view('Auth.postEditForm')->with('post', $post);
    }
}
