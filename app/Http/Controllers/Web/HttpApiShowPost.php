<?php


namespace App\Http\Controllers\Web;


use App\Http\Actions\posts\ActionPostGet;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

/**
 * Class HttpApiShowPost
 * @package App\Http\Controllers\Web
 */
class HttpApiShowPost
{
    /** @var ActionPostGet $action_post_get */
    private $action_post_get;

    /**
     * HttpApiShowPost constructor.
     * @param ActionPostGet $action_post_get
     */
    public function __construct(ActionPostGet $action_post_get)
    {
        $this->action_post_get = $action_post_get;
    }

    /**
     * @param int $post_id
     * @return Application|Factory|View
     */
    public function __invoke(int $post_id)
    {
        $post = $this->action_post_get->__invoke($post_id);

        return view('viewPost')->with('post', $post);
    }

}
