<?php


namespace App\Http\Controllers\Web;


use App\Http\Actions\ActionListUsers;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

/**
 * Class HttpWebLoginForm
 * @package App\Http\Controllers\Web
 */
class HttpWebLoginForm
{
    /** @var ActionListUsers $action_list_users */
    private $action_list_users;

    /**
     * HttpWebLoginForm constructor.
     * @param ActionListUsers $action_list_users
     */
    public function __construct(ActionListUsers $action_list_users)
    {
        $this->action_list_users = $action_list_users;
    }

    /**
     * @return Application|Factory|View
     */
    public function __invoke()
    {
        #1 Getting list of users (not paginated)
        $users = $this->action_list_users->__invoke();

        return view('Auth.login')->with('users', $users);
    }
}
