<?php
namespace App\Http\Controllers\Web;


use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

/**
 * Class HttpWebAddPostForm
 * @package App\Http\Controllers\Web
 */
class HttpWebAddPostForm
{

    /**
     * @return Application|Factory|View
     */
    public function __invoke()
    {
        return view('Auth.postAddForm');
    }
}
