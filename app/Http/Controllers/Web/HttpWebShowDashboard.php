<?php
namespace App\Http\Controllers\Web;


use App\Http\Actions\posts\ActionPostsList;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

/**
 * Class HttpWebShowDashboard
 * @package App\Http\Controllers\Web
 */
class HttpWebShowDashboard
{
    /** @var ActionPostsList */
    private $action_posts_list;

    /**
     * HttpWebShowDashboard constructor.
     * @param ActionPostsList $action_posts_list
     */
    public function __construct(ActionPostsList $action_posts_list)
    {
        $this->action_posts_list = $action_posts_list;
    }

    /**
     * @return Application|Factory|View
     */
    public function __invoke()
    {
        #1 getting list of posts (not paginated)
        $posts = $this->action_posts_list->__invoke();

        return view('Auth.dashboard')->with('posts', $posts);
    }
}
