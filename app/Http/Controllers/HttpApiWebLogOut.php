<?php


namespace App\Http\Controllers;


use App\Http\Actions\ActionUserLogOut;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

/**
 * Class HttpApiWebLogOut
 * @package App\Http\Controllers
 */
class HttpApiWebLogOut
{
    /** @var ActionUserLogOut $action_user_log_out */
    private $action_user_log_out;

    /**
     * HttpApiWebLogOut constructor.
     * @param ActionUserLogOut $action_user_log_out
     */
    public function __construct(ActionUserLogOut $action_user_log_out)
    {
        $this->action_user_log_out = $action_user_log_out;
    }

    /**
     * @return Application|RedirectResponse|Redirector
     */
    public function __invoke()
    {
        #1 log user out
        $this->action_user_log_out->__invoke();

        #2 return to login page
        return redirect('login');
    }

}
