<?php
namespace App\Http\Actions;


/**
 * Class ActionUserLogOut
 * @package App\Http\Actions
 */
class ActionUserLogOut
{

    /**
     *
     */
    public function __invoke()
    {
        auth()->logout();
    }
}
