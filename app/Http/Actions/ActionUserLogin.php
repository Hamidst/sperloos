<?php
namespace App\Http\Actions;


use Illuminate\Support\Facades\Auth;

/**
 * Class ActionUserLogin
 * @package App\Http\Actions
 */
class ActionUserLogin
{
    // $credentials array has an email and password
    /**
     * @param array $credentials
     * @return bool
     */
    public function __invoke(array $credentials)
    {
        if (Auth::attempt($credentials)) {
            return true;
        }
        return false;
    }
}
