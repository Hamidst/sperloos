<?php


namespace App\Http\Actions\posts;


use App\Repositories\RepoPost;

/**
 * Class ActionPostsUpdate
 * @package App\Http\Actions\posts
 */
class ActionPostsUpdate
{
    /** @var RepoPost $repo_post */
    private $repo_post;

    /**
     * ActionPostsDelete constructor.
     * @param RepoPost $repo_post
     */
    public function __construct(RepoPost $repo_post)
    {
        $this->repo_post = $repo_post;
    }

    /**
     * @param int $post_id
     * @param array $post
     */
    public function __invoke(int $post_id, array $post)
    {
        return $this->repo_post->update($post_id, $post);
    }
}
