<?php


namespace App\Http\Actions\posts;


use App\Repositories\RepoPost;

/**
 * Class ActionPostsList
 * @package App\Http\Actions\posts
 */
class ActionPostsList
{
    /** @var RepoPost $repo_post */
    private $repo_post;

    /**
     * ActionPostsList constructor.
     * @param RepoPost $repo_post
     */
    public function __construct(RepoPost $repo_post)
    {
        $this->repo_post = $repo_post;
    }

    /**
     *
     */
    public function __invoke()
    {
        return $this->repo_post->list();
    }

}
