<?php


namespace App\Http\Actions\posts;


use App\Repositories\RepoPost;

/**
 * Class ActionPostsSave
 * @package App\Http\Actions\posts
 */
class ActionPostsSave
{
    /** @var RepoPost $repo_post */
    private $repo_post;

    /**
     * ActionPostsDelete constructor.
     * @param RepoPost $repo_post
     */
    public function __construct(RepoPost $repo_post)
    {
        $this->repo_post = $repo_post;
    }

    /**
     * @param array $post
     */
    public function __invoke(array $post)
    {
        $post['user_id'] = auth()->user()->id;
        return $this->repo_post->save($post);
    }
}
