<?php


namespace App\Http\Actions\posts;


use App\Repositories\RepoPost;

/**
 * Class ActionPostsDelete
 * @package App\Http\Actions\posts
 */
class ActionPostsDelete
{
    /** @var RepoPost $repo_post */
    private $repo_post;

    /**
     * ActionPostsDelete constructor.
     * @param RepoPost $repo_post
     */
    public function __construct(RepoPost $repo_post)
    {
        $this->repo_post = $repo_post;
    }

    /**
     * @param int $post_id
     */
    public function __invoke(int $post_id)
    {
        return $this->repo_post->delete($post_id);
    }

}
