<?php


namespace App\Http\Actions\posts;


use App\Models\Post;
use Illuminate\Http\UploadedFile;

class ActionUploadPostThumbnail
{
    public function __invoke(Post $post, UploadedFile $thumbnail)
    {
        $post->addMediaFromRequest('thumbnail')->toMediaCollection('thumbnail');
    }
}
