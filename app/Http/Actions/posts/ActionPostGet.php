<?php


namespace App\Http\Actions\posts;


use App\Repositories\RepoPost;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ActionGetPost
 * @package App\Http\Actions\posts
 */
class ActionPostGet
{
    /** @var RepoPost $repo_post */
    private $repo_post;

    /**
     * ActionGetPost constructor.
     * @param RepoPost $repo_post
     */
    public function __construct(RepoPost $repo_post)
    {
        $this->repo_post = $repo_post;
    }

    /**
     * @param int $post_id
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function __invoke(int $post_id)
    {
        return $this->repo_post->get($post_id);
    }
}
