<?php


namespace App\Http\Actions;


use App\Repositories\RepoUser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class ActionListUsers
 * @package App\Http\Actions
 */
class ActionListUsers
{
    /** @var RepoUser $repo_user */
    private $repo_user;

    /**
     * ActionListUsers constructor.
     * @param RepoUser $repo_user
     */
    public function __construct(RepoUser $repo_user)
    {
        $this->repo_user = $repo_user;
    }

    /**
     * @return Builder[]|Collection
     */
    public function __invoke()
    {
        return $this->repo_user->list();
    }

}
