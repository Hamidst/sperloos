<?php

namespace App\Http\Middleware;

use App\Models\Post;
use Closure;
use Illuminate\Http\Request;

class PostManipulation
{
    /**
     * Handle an incoming request.
     * Using this middleware we check if user can create, update or delete a post
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->user()->can('manipulatePost', Post::class))
            return $next($request);

        return redirect('dashboard');
    }
}
