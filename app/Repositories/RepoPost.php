<?php
namespace App\Repositories;


use App\Models\Post;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RepoPost
 * @package App\Repositories
 */
class RepoPost
{
    /** @var Post $model_post */
    private $model_post;

    /**
     * RepoPost constructor.
     * @param Post $model_post
     */
    public function __construct(Post $model_post)
    {
        $this->model_post = $model_post;
    }

    /**
     * @return Builder[]|Collection
     */
    public function list()
    {
        return $this->model_post->newQuery()
            ->with(['user'])
            ->orderBy('id', 'desc')
            ->get();
    }

    /**
     * @param array $post_data
     * @return Post
     */
    public function save(array $post_data)
    {
        $post = $this->model_post->newInstance();

        $post->fill(
            $post_data
        )->save();

        return $post;
    }

    /**
     * @param int $post_id
     * @param array $post
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function update(int $post_id, array $post)
    {
        $this->model_post->newInstance()
            ->newQuery()
            ->where('id', $post_id)
            ->update($post);

        return $this->get($post_id);
    }

    /**
     * @param int $post_id
     * @return mixed
     */
    public function delete(int $post_id)
    {
        return $this->model_post->newQuery()
            ->find($post_id)
            ->delete();
    }

    /**
     * @param int $post_id
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function get(int $post_id)
    {
        return $this->model_post->newQuery()
            ->find($post_id);
    }
}
