<?php


namespace App\Repositories;


use App\DataObjects\DOUserLogin;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class RepoUser
 * @package App\Repositories
 */
class RepoUser
{
    /** @var User $model_user */
    protected $model_user;

    /**
     * RepoUser constructor.
     * @param User $model_user
     */
    public function __construct(User $model_user)
    {
        $this->model_user = $model_user;
    }

    /**
     * @return Builder[]|Collection
     */
    public function list()
    {
        return $this->model_user->newQuery()
            ->get();
    }

}
