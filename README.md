1. after cloning the project run this command:
```angular2html
cp .env.example .env
docker-compose up -d
docker-compose exec sapp bash
composer install
php artisan storage:link
php artisan migrate
php artisan db:seed

chown -R www-data:www-data /var/www
chmod -R ug+w /var/www/storage
```

Note that you need a good vpn to use docker images.

This simple project has been created for sperloos company's test.
This project is temporary availabel from:
http://103.215.221.157:8080/

Gitlab Repo:
https://gitlab.com/Hamidst/sperloos.git
