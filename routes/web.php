<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

// Global routes
Route::get('login', '\App\Http\Controllers\Web\HttpWebLoginForm')->name('loginForm');
Route::post('login', '\App\Http\Controllers\HttpWebLogin')->name('login');
Route::get('logout', '\App\Http\Controllers\HttpApiWebLogOut')->name('logOut');

// Available routes tp all authenticated users
Route::middleware(['auth'])->group(function () {
    Route::get('dashboard', '\App\Http\Controllers\Web\HttpWebShowDashboard')
        ->name('dashboardPage');

    Route::get('{post_id}', '\App\Http\Controllers\Web\HttpApiShowPost')
        ->name('showPost')
        ->where('post_id', '[0-9]+');
});


// Available routes for authenticated users who can manipulate posts
Route::middleware(['auth', 'allowedToManipulatePost'])->prefix('posts/')->group(function () {
    Route::post('delete/{post_id}', '\App\Http\Controllers\HttpWebDeletePost')
        ->name('deletePost')
        ->where('post_id', '[0-9]+');

    Route::post('edit/{post_id}', '\App\Http\Controllers\HttpWebEditPost')
        ->name('editPost')
        ->where('post_id', '[0-9]+');

    Route::get('edit/{post_id}', '\App\Http\Controllers\Web\HttpWebEditPostForm')
        ->name('editPostForm')
        ->where('post_id', '[0-9]+');

    Route::get('add', '\App\Http\Controllers\Web\HttpWebAddPostForm')
        ->name('addPostForm');

    Route::post('add', '\App\Http\Controllers\HttpWebAddPost')
        ->name('addPost');
});
